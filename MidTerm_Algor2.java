
import java.util.Scanner;

public class MidTerm_Algor2{
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int sum = 0;
        String S = kb.next();
        for(int i=0;i<S.length();i++){
            sum+=checkInt(S.substring(i, i+1))*Math.pow(10, S.length()-i-1);
        }
        System.out.println(sum);
    }
    public static int checkInt(String s){
        if(s.equals("0")){
            return 0;
        }else if(s.equals("1")){
            return 1;
        }else if(s.equals("2")){
            return 2;
        }else if(s.equals("3")){
            return 3;
        }else if(s.equals("4")){
            return 4;
        }else if(s.equals("5")){
            return 5;
        }else if(s.equals("6")){
            return 6;
        }else if(s.equals("7")){
            return 7;
        }else if(s.equals("8")){
            return 8;
        }else if(s.equals("9")){
            return 9;
        }
        return 0;
    }
}